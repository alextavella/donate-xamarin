﻿using System;
using UIKit;

using XF.Donate.API;

using Foundation;

namespace XF.Donate.iOS
{
    public class Call_iOS : ICall
    {
        public bool Call(string number)
        {
            var url = new NSUrl("tel:" + number);
            var supported = !UIApplication.SharedApplication.OpenUrl(url);

            if (!supported)
            {
                var av = new UIAlertView("Not supported", "Scheme 'tel:' is not supported on this device", null, "OK", null);
                av.Show();
            };

            return supported;
        }
    }
}
