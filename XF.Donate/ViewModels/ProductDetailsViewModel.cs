﻿using Xamarin.Forms;

using XF.Donate.Models;
using XF.Donate.API;

namespace XF.Donate.ViewModels
{
    public class ProductDetailsViewModel : BaseViewModel
    {
        public ProductModel Product{ get; set; }

        public ProductDetailsViewModel(ProductModel product)
        {
            this.Product = product;
            this.Title = product.Name;
        }

        public void Call()
        {
            ICall phone = DependencyService.Get<ICall>();
            string number = this.Product.Phone.Number;

            if (phone != null && !string.IsNullOrEmpty(number))
                phone.Call(number);
        }
    }
}
