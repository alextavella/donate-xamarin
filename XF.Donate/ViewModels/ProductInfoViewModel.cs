﻿namespace XF.Donate.ViewModels
{
    public class ProductInfoModel {
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class ProductInfoViewModel : StepperViewModel
    {
        ProductInfoModel info;
        public ProductInfoModel Info
        {
            get { return info; }
            set { SetProperty(ref info, value); }
        }

        public ProductInfoViewModel()
            : base("Product Info")
        {
            this.Info = new ProductInfoModel();

            if (App.IsDebug)
            {
                this.Info = new ProductInfoModel()
                {
                    Name = "Product Name",
                    Description = "Lorem Ipsum.."
                };
            }
        }
    }
}
