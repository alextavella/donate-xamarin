﻿using System;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;

using XF.Donate.Models;
using XF.Donate.Services.Contracts;
using XF.Donate.Services;
using XF.Donate.Services.Exceptions;
using XF.Donate.Utils;

namespace XF.Donate.ViewModels
{
    public class ProductResumeModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public AddressModel Address { get; set; }
        public PhoneModel Phone { get; set; }
        public CategoryModel Category { get; set; }

        public CameraItem Image
        {
            get
            {
                return CameraUtil.GetByCategory(Category.Code);
            }
        }
    }

    public class ResumeViewModel : StepperViewModel
    {
        public IProductService Service => DependencyService.Get<ProductService>();

        public Command RegisterCommand { get; private set; }

        ProductResumeModel _product;
        public ProductResumeModel Product
        {
            get { return _product; }
            set { SetProperty(ref _product, value); }
        }

        public ResumeViewModel()
            : base("Resume")
        {
            this.RegisterCommand = new Command(async () => await ExecuteRegisterCommand());

            try
            {
                var product = RegisterUtil.Product;
                var address = RegisterUtil.Address;
                var phone = RegisterUtil.Phone;
                var category = RegisterUtil.Category;

                this.Product = new ProductResumeModel()
                {
                    Name = product.Name,
                    Description = product.Description,
                    Address = address,
                    Phone = phone,
                    Category = category
                };
            }
            catch
            {
                Debug.WriteLine("Product invalid!");
            }
        }

        async Task ExecuteRegisterCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                var product = RegisterUtil.Product;
                var address = RegisterUtil.Address;
                var phone = RegisterUtil.Phone;
                var category = RegisterUtil.Category;

                var save = new ProductRegisterModel()
                {
                    Name = product.Name,
                    Description = product.Description,
                    Address = address.Id,
                    Phone = phone.Id,
                    Category = category.Id
                };

                var saved = await this.Service.Register(save);
                MessagingCenter.Send<ResumeViewModel>(new ResumeViewModel(), Events.Saved);
            }
            catch (ForbiddenException ex)
            {
                MessagingCenter.Send<MainPage>(new MainPage(), Events.NeedLogon);
                Debug.WriteLine(ex);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
