﻿using System;

namespace XF.Donate.ViewModels
{
    public class StepperViewModel : BaseViewModel
    {
        bool next = false;
        public bool Next
        {
            get { return next; }
            set { SetProperty(ref next, value); }
        }

        public StepperViewModel(string title)
        {
            Title = title;
        }
    }
}
