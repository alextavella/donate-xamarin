﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;

using XF.Donate.Services.Contracts;
using XF.Donate.Models;
using XF.Donate.Services;
using XF.Donate.Utils;
using System.Linq;

namespace XF.Donate.ViewModels
{
    public class ProductViewModel : BaseViewModel
    {
        public IProductService Service => DependencyService.Get<ProductService>();

        public ObservableCollection<ProductModel> Products { get; set; }
        public Command LoadCommand { get; set; }

        public ProductViewModel()
        {
            Title = "Products";
            Products = new ObservableCollection<ProductModel>();
            LoadCommand = new Command(async () => await ExecuteLoadCommand());
        }

        async Task ExecuteLoadCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Products.Clear();
                ProductsModel result = await Service.List();

                foreach (var item in result.Products.Where(x => x.IsValid))
                {
                    Products.Add(item);
                }

                MessagingCenter.Send<CategoryViewModel>(new CategoryViewModel(), Events.Bind);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
