﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using XF.Donate.Services;
using XF.Donate.Services.Contracts;
using XF.Donate.Utils;
using XF.Donate.Services.Exceptions;

namespace XF.Donate.ViewModels
{
    public class UserViewModel : BaseViewModel
    {
        public IAuthService Service => DependencyService.Get<AuthService>();

        public Command LoginCommand { get; set; }
        public Command RegisterCommand { get; set; }

        public static string Error { get; set; }

        LoginModel model;
        public LoginModel Login
        {
            get { return model; }
            set { SetProperty(ref model, value); }
        }

        RegisterModel register;
        public RegisterModel Register
        {
            get { return register; }
            set { SetProperty(ref register, value); }
        }

        public UserViewModel()
        {
            Title = "Login";

            Login = new LoginModel()
            {
                Email = "test@example.com",
                Password = "123456"
            };

            Register = new RegisterModel();

            if (App.IsDebug)
            {
                Register = new RegisterModel()
                {
                    FirstName = "Alex",
                    LastName = "Tavella",
                    Email = "alextavella@outlook.com",
                    Password = "asdasd"
                };
            }

            LoginCommand = new Command(async () => await ExecuteLoginCommand());
            RegisterCommand = new Command(async () => await ExecuteRegisterCommand());
        }

        async Task ExecuteLoginCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                await Service.Login(Login);
                MessagingCenter.Send<UserViewModel>(new UserViewModel(), Events.Logon);
            }
            catch (BusinessException ex)
            {
                Error = ex.Message;
                MessagingCenter.Send<UserViewModel>(new UserViewModel(), Events.Error);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        async Task ExecuteRegisterCommand()
        {
            if (IsBusy)
                return;

            try
            {
                await Service.Register(Register);
                MessagingCenter.Send<UserViewModel>(new UserViewModel(), Events.Logon);
            }
            catch (BusinessException ex)
            {
                Error = ex.Message;
                MessagingCenter.Send<UserViewModel>(new UserViewModel(), Events.Error);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
