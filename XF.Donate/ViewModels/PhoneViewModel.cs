﻿using System;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Linq;

using Xamarin.Forms;

using XF.Donate.Models;
using XF.Donate.Services.Contracts;
using XF.Donate.Services;
using XF.Donate.Services.Exceptions;
using XF.Donate.Utils;

namespace XF.Donate.ViewModels
{
    public class PhoneViewModel : StepperViewModel
    {
        public IPhoneService Service => DependencyService.Get<PhoneService>();

        public ObservableCollection<PhoneModel> Phones { get; private set; }

        public Command LoadCommand { get; private set; }
        public Command RegisterCommand { get; private set; }
        public Command RemoveCommand { get; private set; }

        public static string Error { get; set; }

        PhoneModel phone;
        public PhoneModel Phone
        {
            get { return phone; }
            set { SetProperty(ref phone, value); }
        }

        bool isEmpty = false;
        public bool IsEmpty
        {
            get { return isEmpty; }
            set { SetProperty(ref isEmpty, value); }
        }

        public PhoneViewModel()
            : base("Phone")
        {
            this.Phones = new ObservableCollection<PhoneModel>();

            this.LoadCommand = new Command(async () => await ExecuteLoadCommand());
            this.RegisterCommand = new Command(async () => await ExecuteRegisterCommand());
            this.RemoveCommand = new Command(async () => await ExecuteRemoveCommand());

            this.Phone = new PhoneModel();
                
            if (App.IsDebug)
            {
                this.Phone = new PhoneModel()
                {
                    Name = "Comercial",
                    Number = "11987654321"
                };
            }
        }

        async Task ExecuteLoadCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                var result = await this.Service.List();

                IsEmpty = !result.Phones.Any();

                Phones.Clear();
                foreach (var item in result.Phones)
                {
                    Phones.Add(item);
                }

                MessagingCenter.Send<PhoneViewModel>(new PhoneViewModel(), Events.Bind);
            }
            catch (ForbiddenException ex)
            {
                MessagingCenter.Send<MainPage>(new MainPage(), Events.NeedLogon);
                Debug.WriteLine(ex);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        async Task ExecuteRegisterCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                RegisterUtil.Phone = await this.Service.Register(this.Phone);
                MessagingCenter.Send<PhoneViewModel>(new PhoneViewModel(), Events.Saved);
            }
            catch (BusinessException ex)
            {
                Error = ex.Message;
                MessagingCenter.Send<PhoneViewModel>(new PhoneViewModel(), Events.Error);
            }
            catch (ForbiddenException ex)
            {
                MessagingCenter.Send<MainPage>(new MainPage(), Events.NeedLogon);
                Debug.WriteLine(ex);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        async Task ExecuteRemoveCommand()
        {
            try
            {
                var id = Phone.Id;
                await this.Service.Remove(id);
                Phones.Remove(Phones.Single(x => x.Id == id));

                MessagingCenter.Send<PhoneViewModel>(new PhoneViewModel(), Events.Removed);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }
    }
}
