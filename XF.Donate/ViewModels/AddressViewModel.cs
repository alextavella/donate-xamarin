﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections.ObjectModel;

using Xamarin.Forms;

using XF.Donate.Models;
using XF.Donate.Services.Contracts;
using XF.Donate.Services;
using XF.Donate.Services.Exceptions;
using XF.Donate.Utils;

namespace XF.Donate.ViewModels
{
    public class AddressViewModel : StepperViewModel
    {
        public IAddressService Service => DependencyService.Get<AddressService>();

        public ObservableCollection<AddressModel> Addresses { get; set; }

        public Command LoadCommand { get; set; }
        public Command RegisterCommand { get; private set; }
        public Command RemoveCommand { get; private set; }
        public Command SearchCommand { get; private set; }

        AddressModel address;
        public AddressModel Address
        {
            get { return address; }
            set { SetProperty(ref address, value); }
        }

        bool isEmpty = false;
        public bool IsEmpty
        {
            get { return isEmpty; }
            set { SetProperty(ref isEmpty, value); }
        }

        public AddressViewModel()
            : base("Address")
        {
            this.Addresses = new ObservableCollection<AddressModel>();

            this.LoadCommand = new Command(async () => await ExecuteLoadCommand());
            this.RegisterCommand = new Command(async () => await ExecuteRegisterCommand());
            this.RemoveCommand = new Command(async () => await ExecuteRemoveCommand());
            this.SearchCommand = new Command(async () => await ExecuteSearchCommand());

            this.Address = new AddressModel();

            if (App.IsDebug)
            {
                this.Address = new AddressModel()
                {
                    Alias = "Residencia",
                    Cep = "09862340",
                    Number = "1234"
                };
            }
        }

        async Task ExecuteLoadCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                var result = await this.Service.List();

                IsEmpty = !result.Addresses.Any();

                Addresses.Clear();
                foreach(var item in result.Addresses) {
                    Addresses.Add(item);
                }

                MessagingCenter.Send<AddressViewModel>(new AddressViewModel(), Events.Bind);
            }
            catch (ForbiddenException ex)
            {
                MessagingCenter.Send<MainPage>(new MainPage(), Events.NeedLogon);
                Debug.WriteLine(ex);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        async Task ExecuteRegisterCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                RegisterUtil.Address = await this.Service.Register(this.Address);
                MessagingCenter.Send<PhoneViewModel>(new PhoneViewModel(), Events.Saved);
            }
            catch (ForbiddenException ex)
            {
                MessagingCenter.Send<MainPage>(new MainPage(), Events.NeedLogon);
                Debug.WriteLine(ex);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        async Task ExecuteRemoveCommand()
        {
            try
            {
                var id = Address.Id;
                await this.Service.Remove(id);
                Addresses.Remove(Addresses.Single(x => x.Id == id));

                MessagingCenter.Send<PhoneViewModel>(new PhoneViewModel(), Events.Removed);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        async Task ExecuteSearchCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                AddressCepModel cepModel = await this.Service.SearchByCep(this.Address.Cep);

                if (cepModel != null)
                {
                    var autoComplete = new AddressModel()
                    {
                        Alias = this.Address.Alias,
                        Country = this.Address.Country,
                        Number = this.Address.Number,
                        Complement = this.Address.Complement,

                        Cep = cepModel.Cep,
                        Street = cepModel.Street,
                        Neighborhood = cepModel.Neighborhood,
                        City = cepModel.City,
                        State = cepModel.State
                    };

                    this.Address = autoComplete;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
