﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using XF.Donate.Services;
using XF.Donate.Services.Contracts;
using XF.Donate.Models;
using XF.Donate.Utils;

namespace XF.Donate.ViewModels
{
    public class CategoryViewModel : BaseViewModel
    {
        public ICategoryService Service => DependencyService.Get<CategoryService>();

        public ObservableCollection<CategoryModel> Categories { get; set; }
        public Command LoadCommand { get; set; }

        public CategoryViewModel()
        {
            Title = "Categories";
            Categories = new ObservableCollection<CategoryModel>();
            LoadCommand = new Command(async () => await ExecuteLoadCommand());
        }

        async Task ExecuteLoadCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Categories.Clear();
                CategoriesModel result = await Service.List();
                foreach (var item in result.Categories) {
                    Categories.Add(item);
                }

                MessagingCenter.Send<CategoryViewModel>(new CategoryViewModel(), Events.Bind);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
