﻿using System;
using System.Linq;
using System.Collections.Generic;

using Xamarin.Forms;

using XF.Donate.ViewModels;
using XF.Donate.Views;
using XF.Donate.Utils;
using XF.Donate.Services.Exceptions;

namespace XF.Donate
{
    public partial class LoginPage : ContentPage
    {
        UserViewModel viewModel;

        public LoginPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new UserViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<UserViewModel>(this, Events.Logon,
                (sender) => 
            {
                Navigation.PushAsync(new CategoryPage());
            });

            MessagingCenter.Subscribe<UserViewModel>(this, Events.Error,
                (sender) =>
                {
                    DisplayAlert("Error", UserViewModel.Error, "OK");
                });
        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Unsubscribe<UserViewModel>(this, Events.Logon);
            MessagingCenter.Unsubscribe<UserViewModel>(this, Events.Error);
            base.OnDisappearing();
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CategoryPage());
        }

        public void Login_Clicked(object sender, EventArgs e) {
            try
            {
                if (this.Valid(this.viewModel.Login))
                    this.viewModel.LoginCommand.Execute(null);
            }
            catch (BusinessException ex)
            {
                DisplayAlert("Error", ex.Message, "OK");
            }
        }

        public async void Register_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new RegisterPage());
        }

        private bool Valid(LoginModel model)
        {
            bool valid = true;

            List<string> fields = new List<string>();

            if (string.IsNullOrWhiteSpace(model.Email))
                fields.Add("Email");
            if (string.IsNullOrWhiteSpace(model.Password))
                fields.Add("Password");

            if (fields.Any())
            {
                DisplayAlert("Validation", $"{string.Join(",", fields)} is required.", "OK");
                return false;
            }

            return valid;
        }
    }
}
