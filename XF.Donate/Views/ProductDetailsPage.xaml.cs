﻿using Xamarin.Forms;

using XF.Donate.ViewModels;
using XF.Donate.API;

namespace XF.Donate.Views
{
    public partial class ProductDetailsPage : ContentPage
    {
        ProductDetailsViewModel viewModel;

        public ProductDetailsPage(ProductDetailsViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
        }

        public void Call_Clicked(object sender, System.EventArgs e)
        {
            this.viewModel.Call();
        }
    }
}
