﻿using System;

using Xamarin.Forms;

using XF.Donate.Services;
using XF.Donate.Models;
using XF.Donate.ViewModels;

namespace XF.Donate.Views
{
    public partial class ProductsPage : ContentPage
    {
        ProductViewModel viewModel;

        public ProductsPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new ProductViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Products.Count == 0)
                viewModel.LoadCommand.Execute(null);
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as ProductModel;
            if (item == null)
                return;

            await Navigation.PushAsync(new ProductDetailsPage(new ProductDetailsViewModel(item)));

            ItemsListView.SelectedItem = null;
        }

        public async void Add_Clicked(object sender, System.EventArgs e)
        {
            if (AuthService.IsAuthenticated())
            {
                await Navigation.PushAsync(new CategoryPage());
            }
            else
            {
                await Navigation.PushAsync(new LoginPage());
            }
        }
    }
}
