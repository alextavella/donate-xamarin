﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using XF.Donate.Utils;
using XF.Donate.Models;
using XF.Donate.ViewModels;

namespace XF.Donate.Views
{
    public partial class PhonePage : ContentPage
    {
        PhoneViewModel viewModel;

        public PhonePage()
        {
            InitializeComponent();

            BindingContext = this.viewModel = new PhoneViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Phones.Count == 0)
                viewModel.LoadCommand.Execute(null);
        }

        public async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as PhoneModel;
            if (item == null)
                return;

            RegisterUtil.Phone = item;
            await Navigation.PushAsync(new ResumePage());

            ItemsListView.SelectedItem = null;
        }

        public void OnDelete(object sender, System.EventArgs e)
        {
            var mi = ((MenuItem)sender);
            PhoneModel model = ((PhoneModel)mi.CommandParameter);

            viewModel.Phone = model;
            viewModel.RemoveCommand.Execute(null);
        }

        public async void Add_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new PhoneRegisterPage());
        }
    }
}
