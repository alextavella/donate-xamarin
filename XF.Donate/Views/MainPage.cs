﻿using System;

using Xamarin.Forms;
using XF.Donate.Utils;
using XF.Donate.Views;

namespace XF.Donate
{
    public class MainPage : TabbedPage
    {
        public MainPage()
        {
            Page 
                itemsPage, 
			          aboutPage = null;

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    itemsPage = new NavigationPage(new ProductsPage())
                    {
                        Title = "Products"
                    };
                    aboutPage = new NavigationPage(new AboutPage())
                    {
                        Title = "About"
                    };
                    itemsPage.Icon = "tab_feed.png";
                    aboutPage.Icon = "tab_about.png";
                    break;
                default:
                    itemsPage = new ProductsPage()
                    {
                        Title = "Products"
                    };
                    aboutPage = new AboutPage()
                    {
                        Title = "About"
                    };
                    break;
            }

            Children.Add(itemsPage);
            //Children.Add(aboutPage);

            Title = Children[0].Title;

            // Login Require
            MessagingCenter.Subscribe<MainPage>(this, Events.NeedLogon,
                (sender) =>
                {
                    Navigation.PushAsync(new LoginPage());
                });
        }

        protected override void OnCurrentPageChanged()
        {
            base.OnCurrentPageChanged();
            Title = CurrentPage?.Title ?? string.Empty;
        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Unsubscribe<MainPage>(this, Events.NeedLogon);
            base.OnDisappearing();
        }
    }
}
