﻿using System;
using System.Linq;
using System.Collections.Generic;

using Xamarin.Forms;

using XF.Donate.ViewModels;
using XF.Donate.Utils;
using XF.Donate.Models;

namespace XF.Donate.Views
{
    public partial class AddressPage : ContentPage
    {
        AddressViewModel viewModel;

        public AddressPage()
        {
            InitializeComponent();

            BindingContext = this.viewModel = new AddressViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Addresses.Count == 0)
                viewModel.LoadCommand.Execute(null);
        }

        public async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as AddressModel;
            if (item == null)
                return;

            RegisterUtil.Address = item;
            await Navigation.PushAsync(new PhonePage());

            ItemsListView.SelectedItem = null;
        }

        public void OnDelete(object sender, System.EventArgs e)
        {
            var mi = ((MenuItem)sender);
            AddressModel model = ((AddressModel)mi.CommandParameter);

            viewModel.Address = model;
            viewModel.RemoveCommand.Execute(null);
        }

        public async void Add_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new AddressRegisterPage());
        }
    }
}
