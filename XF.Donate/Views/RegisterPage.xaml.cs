﻿using System;
using System.Linq;
using System.Collections.Generic;

using Xamarin.Forms;

using XF.Donate.ViewModels;
using XF.Donate.Utils;
using XF.Donate.Services.Exceptions;

namespace XF.Donate.Views
{
    public partial class RegisterPage : ContentPage
    {
        UserViewModel viewModel;

        public RegisterPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new UserViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<UserViewModel>(this, Events.Logon,
                (sender) =>
                {
                    Navigation.PushAsync(new CategoryPage());
            });

            MessagingCenter.Subscribe<UserViewModel>(this, Events.Error,
                (sender) =>
                {
                    DisplayAlert("Error", UserViewModel.Error, "OK");
                });
        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Unsubscribe<UserViewModel>(this, Events.Logon);
            MessagingCenter.Unsubscribe<UserViewModel>(this, Events.Error);
            base.OnDisappearing();
        }

        public void Register_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (this.Valid(this.viewModel.Register))
                    viewModel.RegisterCommand.Execute(null);
            }
            catch (BusinessException ex)
            {
                DisplayAlert("Error", ex.Message, "OK");
            }
        }

        private bool Valid(RegisterModel model)
        {
            bool valid = true;

            List<string> fields = new List<string>();

            if (string.IsNullOrWhiteSpace(model.Email))
                fields.Add("Email");
            if (string.IsNullOrWhiteSpace(model.Password))
                fields.Add("Password");
            if (string.IsNullOrWhiteSpace(model.FirstName))
                fields.Add("First Name");
            if (string.IsNullOrWhiteSpace(model.LastName))
                fields.Add("Last Name");

            if (fields.Any())
            {
                DisplayAlert("Validation", $"{string.Join(",", fields)} is required.", "OK");
                return false;
            }

            return valid;
        }
    }
}
