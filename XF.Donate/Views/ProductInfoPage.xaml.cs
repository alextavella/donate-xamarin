﻿using System;
using System.Linq;
using System.Collections.Generic;

using Xamarin.Forms;

using XF.Donate.ViewModels;
using XF.Donate.Utils;

namespace XF.Donate.Views
{
    public partial class ProductInfoPage : ContentPage
    {
        public ProductInfoViewModel viewModel;

        public ProductInfoPage()
        {
            InitializeComponent();

            BindingContext = this.viewModel = new ProductInfoViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            this.viewModel.Next = true;
        }

        public async void Next_Clicked(object sender, EventArgs e)
        {
            if (this.Valid(this.viewModel.Info))
            {
                RegisterUtil.Product.Name = this.viewModel.Info.Name;
                RegisterUtil.Product.Description = this.viewModel.Info.Description;
                await Navigation.PushAsync(new AddressPage());
            }
        }

        private bool Valid(ProductInfoModel model)
        {
            bool valid = true;

            List<string> fields = new List<string>();

            if (string.IsNullOrWhiteSpace(model.Name))
                fields.Add("Name");
            if (string.IsNullOrWhiteSpace(model.Description))
                fields.Add("Description");

            if (fields.Any())
            {
                DisplayAlert("Validation", $"{string.Join(",", fields)} is required.", "OK");
                return false;
            }

            return valid;
        }
    }
}
