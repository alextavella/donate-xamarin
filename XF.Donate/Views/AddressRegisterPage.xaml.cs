﻿using System;
using System.Linq;
using System.Collections.Generic;

using Xamarin.Forms;

using XF.Donate.ViewModels;
using XF.Donate.Utils;
using XF.Donate.Models;
using System.Linq;

namespace XF.Donate.Views
{
    public partial class AddressRegisterPage : ContentPage
    {
        AddressViewModel viewModel;

        public AddressRegisterPage()
        {
            InitializeComponent();

            BindingContext = this.viewModel = new AddressViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<PhoneViewModel>(this, Events.Saved,
                (sender) =>
                {
                    Navigation.PushAsync(new PhonePage());
                });
        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Unsubscribe<PhoneViewModel>(this, Events.Saved);
            base.OnDisappearing();
        }

        public void Cep_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            this.viewModel.SearchCommand.Execute(null);
        }

        public void Save_Clicked(object sender, EventArgs args)
        {
            if (this.Valid(this.viewModel.Address))
                this.viewModel.RegisterCommand.Execute(null);
        }

        private bool Valid(AddressModel model)
        {
            bool valid = true;

            List<string> fields = new List<string>();

            if (string.IsNullOrWhiteSpace(model.Alias))
                fields.Add("Name");
            if (string.IsNullOrWhiteSpace(model.Cep))
                fields.Add("Cep");
            if (string.IsNullOrWhiteSpace(model.Number))
                fields.Add("Number");
            if (string.IsNullOrWhiteSpace(model.Street))
                fields.Add("Street");
            if (string.IsNullOrWhiteSpace(model.Neighborhood))
                fields.Add("Neighborhood");
            if (string.IsNullOrWhiteSpace(model.City))
                fields.Add("City");
            if (string.IsNullOrWhiteSpace(model.State))
                fields.Add("State");

            if (fields.Any())
            {
                DisplayAlert("Validation", $"{string.Join(",", fields)} is required.", "OK");
                return false;
            }

            return valid;
        }
    }
}
