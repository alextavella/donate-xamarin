﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using XF.Donate.Utils;
using XF.Donate.ViewModels;

namespace XF.Donate.Views
{
    public partial class CameraPage : ContentPage
    {
        StepperViewModel viewModel;

        public CameraPage()
        {
            InitializeComponent();

            BindingContext = this.viewModel = new StepperViewModel("Camera");
        }

        public void Picture_Clicked(object sender, System.EventArgs e)
        {
            var code = RegisterUtil.Category.Code;
            var imageUrl = CameraUtil.GetByCategory(code).Url;

            this.imgFoto.Source = ImageSource.FromUri(new Uri(imageUrl));
            this.viewModel.Next = true;
        }

        public async void Next_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new ProductInfoPage());
        }

        //private void btnCamera_Clicked(object sender, EventArgs e)
        //{
        //    ICamera capturar = DependencyService.Get<ICamera>();
        //    capturar.CapturarFoto();

        //    MessagingCenter.Subscribe<ICamera, string>(this, "cameraFoto",
        //        (objeto, image) =>
        //        {
        //            this.imgFoto.Source = ImageSource.FromFile(image);
        //        });
        //}

        //private void btnSelecionar_Clicked(object sender, EventArgs e)
        //{
        //    ICamera capturar = DependencyService.Get<ICamera>();
        //    capturar.SelecionarFoto();

        //    MessagingCenter.Subscribe<ICamera, string>(this, "cameraFoto",
        //        (objeto, image) =>
        //        {
        //            this.imgFoto.Source = ImageSource.FromFile(image);
        //        });
        //}
    }
}
