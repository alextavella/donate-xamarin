﻿using System;
using System.Linq;
using System.Collections.Generic;

using Xamarin.Forms;

using XF.Donate.ViewModels;
using XF.Donate.Utils;
using XF.Donate.Models;

namespace XF.Donate.Views
{
    public partial class PhoneRegisterPage : ContentPage
    {
        PhoneViewModel viewModel;

        public PhoneRegisterPage()
        {
            InitializeComponent();

            BindingContext = this.viewModel = new PhoneViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            viewModel.LoadCommand.Execute(null);

            MessagingCenter.Subscribe<PhoneViewModel>(this, Events.Saved,
                (sender) =>
                {
                    Navigation.PushAsync(new ResumePage());
                });

            MessagingCenter.Subscribe<PhoneViewModel>(this, Events.Error,
                (sender) =>
                {
                    DisplayAlert("Error", PhoneViewModel.Error, "OK");
                });
        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Unsubscribe<PhoneViewModel>(this, Events.Saved);
            MessagingCenter.Unsubscribe<PhoneViewModel>(this, Events.Error);
            base.OnDisappearing();
        }

        public void Save_Clicked(object sender, EventArgs args)
        {
            if (this.Valid(this.viewModel.Phone))
                this.viewModel.RegisterCommand.Execute(null);
        }

        private bool Valid(PhoneModel model)
        {
            bool valid = true;

            IList<string> fields = new List<string>();

            if (string.IsNullOrWhiteSpace(model.Name))
                fields.Add("Name");
            if (string.IsNullOrWhiteSpace(model.Number))
                fields.Add("Number");

            if (fields.Any())
            {
                DisplayAlert("Validation", $"{string.Join(",", fields)} is required.", "OK");
                return false;
            }

            return valid;
        }
    }
}
