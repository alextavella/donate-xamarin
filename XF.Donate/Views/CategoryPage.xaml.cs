﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;

using XF.Donate.Models;
using XF.Donate.ViewModels;
using XF.Donate.Utils;
using XF.Donate.Views;

namespace XF.Donate
{
    public partial class CategoryPage : ContentPage
    {
        CategoryViewModel viewModel;

        public CategoryPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new CategoryViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Categories.Count == 0)
                viewModel.LoadCommand.Execute(null);
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as CategoryModel;
            if (item == null)
                return;

            RegisterUtil.Category = item;
            await Navigation.PushAsync(new CameraPage());

            //await Navigation.PushAsync(new ItemDetailPage(new ItemDetailViewModel(item)));

            // Manually deselect item
            ItemsListView.SelectedItem = null;
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NewItemPage());
        }
    }
}
