﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using XF.Donate.ViewModels;
using XF.Donate.Utils;

namespace XF.Donate.Views
{
    public partial class ResumePage : ContentPage
    {
        ResumeViewModel viewModel;

        public ResumePage()
        {
            InitializeComponent();

            BindingContext = this.viewModel = new ResumeViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<ResumeViewModel>(this, Events.Saved,
                (sender) =>
            {
                Navigation.PushAsync(new ProductsPage());
            });
        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Unsubscribe<ResumeViewModel>(this, Events.Saved);
            base.OnDisappearing();
        }

        public void Handle_Clicked(object sender, System.EventArgs e)
        {
            this.viewModel.RegisterCommand.Execute(null);
        }
    }
}
