﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace XF.Donate.Utils
{
    public class CameraItem
    {
        public int Code { get; set; }
        public string Url { get; set; }
    }

    public class CameraUtil
    {
        private static List<CameraItem> Images = new List<CameraItem>() {
            new CameraItem { Url = "http://alextavellaapp-com.umbler.net/images/products/01-telefone.jpg", Code = 1 },
            new CameraItem { Url = "http://alextavellaapp-com.umbler.net/images/products/02-eletrodomesticos.jpg", Code = 2 },
            new CameraItem { Url = "http://alextavellaapp-com.umbler.net/images/products/03-tvs_video.jpg", Code = 3 },
            new CameraItem { Url = "http://alextavellaapp-com.umbler.net/images/products/04-informatica.jpg", Code = 4 },
            new CameraItem { Url = "http://alextavellaapp-com.umbler.net/images/products/05-ar_ventilacao.jpg", Code = 5 },
            new CameraItem { Url = "http://alextavellaapp-com.umbler.net/images/products/06-moveis.jpg", Code = 6 },
            new CameraItem { Url = "http://alextavellaapp-com.umbler.net/images/products/07-eletroportateis.jpg", Code = 7 },
            new CameraItem { Url = "http://alextavellaapp-com.umbler.net/images/products/08-games.jpg", Code = 8 }
        };

        public static CameraItem GetRandom() {
            int random = new Random().Next(0, Images.Count);
            return Images[random];
        }

        public static CameraItem GetByCategory(int categoryCode)
        {
            return Images.Single(x => x.Code == categoryCode);
        }
    }
}
