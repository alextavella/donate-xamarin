﻿using System;
namespace XF.Donate.Utils
{
    public class Events
    {
        public static string NeedLogon = "NeedLogon";
        public static string Logon = "Logon";

        public static string Bind = "Bind";
        public static string Saved = "Saved";
        public static string Removed = "Removed";
        public static string Error = "Error";
    }
}
