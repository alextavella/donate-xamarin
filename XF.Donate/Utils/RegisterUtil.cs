﻿using System;
using XF.Donate.Models;

namespace XF.Donate.Utils
{
    public static class RegisterUtil
    {
        private static ProductModel product;
        public static ProductModel Product
        {
            get
            {
                if (product == null)
                {
                    product = new ProductModel();
                }
                return product;
            }
            set { product = value; }
        }

        public static CategoryModel Category
        {
            get { return Product.Category; }
            set { Product.Category = value; }
        }

        public static PhoneModel Phone
        {
            get { return Product.Phone; }
            set { Product.Phone = value; }
        }

        public static AddressModel Address
        {
            get { return Product.Address; }
            set { Product.Address = value; }
        }
    }
}
