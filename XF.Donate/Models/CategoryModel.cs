﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace XF.Donate.Models
{
    public class CategoryImageModel 
    {   
        //[JsonProperty("base64")]
        //public Stream[] Base64 { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }

    public class CategoryModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("image")]
        public CategoryImageModel Image { get; set; }
    }

    public class CategoriesModel
    {
        [JsonProperty("categories")]
        public IEnumerable<CategoryModel> Categories { get; set; }

        public CategoriesModel()
        {
            Categories = new List<CategoryModel>();
        }
    }
}
