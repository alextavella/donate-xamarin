﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;
using XF.Donate.Utils;

namespace XF.Donate.Models
{
    public class ProductModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("user")]
        public UserModel User { get; set; }

        [JsonProperty("address")]
        public AddressModel Address { get; set; }

        [JsonProperty("phone")]
        public PhoneModel Phone { get; set; }

        [JsonProperty("category")]
        public CategoryModel Category { get; set; }

        public CameraItem Image { 
            get
            {
                return CameraUtil.GetByCategory(Category.Code);
            }
        }

        public bool HasPhone
        {
            get { return Phone != null; }
        }

        public bool HasAddress
        {
            get { return Address != null; }
        }

        public bool IsValid
        {
            get
            {
                return (
                    !string.IsNullOrEmpty(Name) &&
                    !string.IsNullOrEmpty(Description) &&
                    Address != null &&
                    Category != null &&
                    Phone != null
                );
            }
        }
    }

    public class ProductsModel {

        [JsonProperty("products")]
        public IEnumerable<ProductModel> Products { get; set; }

        public ProductsModel(){
            this.Products = new List<ProductModel>();
        }
    }

    public class ProductRegisterModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("category")]
        public string Category { get; set; }
    }
}
