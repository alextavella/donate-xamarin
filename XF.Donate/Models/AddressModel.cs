﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace XF.Donate.Models
{
    public class CountryModel
    {
        [JsonProperty("long_name")]
        public string LongName { get; set; }

        [JsonProperty("short_name")]
        public string ShortName { get; set; }
    }

    public class LocationModel
    {
        [JsonProperty("lng")]
        public double Lng { get; set; }

        [JsonProperty("lat")]
        public double Lat { get; set; }
    }

    public class AddressModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("user")]
        public string UserId { get; set; }

        [JsonProperty("alias")]
        public string Alias { get; set; }

        [JsonProperty("cep")]
        public string Cep { get; set; }

        [JsonProperty("street")]
        public string Street { get; set; }

        [JsonProperty("number")]
        public string Number { get; set; }

        [JsonProperty("complement")]
        public string Complement { get; set; }

        [JsonProperty("neighborhood")]
        public string Neighborhood { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("country")]
        public CountryModel Country { get; set; }

        [JsonProperty("location")]
        public LocationModel Location { get; set; }

        [JsonProperty("active")]
        public bool Active{ get; set; }

        public string Full {
            get
            {
                if (string.IsNullOrEmpty(Complement))
                {
                    return $"{Street}, {Number} {Complement}, {Neighborhood} - {City} - {State}";
                }
                return $"{Street}, {Number}, {Neighborhood} - {City} - {State}";
            }
        }
    }

    public class AddressesModel
    {
        [JsonProperty("addresses")]
        public IEnumerable<AddressModel> Addresses { get; set; }

        public AddressesModel()
        {
            this.Addresses = new List<AddressModel>();
        }
    }

    public class AddressCepModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("cep")]
        public string Cep { get; set; }

        [JsonProperty("street")]
        public string Street { get; set; }

        [JsonProperty("neighborhood")]
        public string Neighborhood { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }
    }
}
