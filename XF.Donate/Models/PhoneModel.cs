﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace XF.Donate.Models
{
    public class PhoneModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("number")]
        public string Number { get; set; }
    }

    public class PhonesModel
    {
        [JsonProperty("phones")]
        public IEnumerable<PhoneModel> Phones { get; set; }

        public PhonesModel()
        {
            Phones = new List<PhoneModel>();
        }
    }
}
