﻿namespace XF.Donate.API
{
    public interface ICall
    {
        bool Call(string number);
    }
}
