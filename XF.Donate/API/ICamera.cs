﻿using System;
namespace XF.Donate.API
{
    public interface ICamera
    {
        void CapturarFoto();
        void SelecionarFoto();
    }
}
