﻿using System;

namespace XF.Donate.Services.Exceptions
{
    public class ForbiddenException : Exception
    {
        public ForbiddenException()
            : base("Authenticate is required")
        {
        }
    }
}
