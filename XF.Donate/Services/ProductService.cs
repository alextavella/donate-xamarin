﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;

using Newtonsoft.Json;
using Plugin.Connectivity;

using XF.Donate.Services.Exceptions;
using XF.Donate.Services.Contracts;
using XF.Donate.Models;

namespace XF.Donate.Services
{
    public class ProductService : IProductService
    {
        HttpClient client;

        ProductsModel products;

        public ProductService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri($"{App.BackendUrl}");

            products = new ProductsModel();
        }

        public async Task<ProductsModel> List()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var json = await client.GetStringAsync($"api/product");
                products = await Task.Run(() => JsonConvert.DeserializeObject<ProductsModel>(json));
            }

            return products;
        }

        public async Task<ProductRegisterModel> Register(ProductRegisterModel model)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var serialized = JsonConvert.SerializeObject(model);
                StringContent content = new StringContent(serialized.ToString(), Encoding.UTF8, "application/json");

                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri($"/api/product"),
                    Method = HttpMethod.Post,
                    Content = content
                };
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", $"{AuthService.AccessToken()}");

                var json = await client.SendAsync(request);
                if (json.IsSuccessStatusCode)
                {
                    var response = json.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<ProductRegisterModel>(response);
                }
                else if (json.StatusCode == System.Net.HttpStatusCode.Forbidden)
                {
                    throw new ForbiddenException();
                }
            }

            return null;
        }
    }
}
