﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Plugin.Connectivity;

using XF.Donate.Services.Contracts;
using XF.Donate.Models;
using XF.Donate.Services.Exceptions;

namespace XF.Donate.Services
{
    public class AuthService : IAuthService
    {
        HttpClient client;

        public static AuthModel AuthModel { get; private set; }

        public AuthService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri($"{App.BackendUrl}");
        }

        public static bool IsAuthenticated()
        {
            return AuthModel != null;
        }

        public static string AccessToken()
        {
            return AuthModel.Token.AccessToken;
        }

        public async Task<AuthModel> Login(string email, string password)
        {
            return await Login(new LoginModel { Email = email, Password = password });
        }

        public async Task<AuthModel> Login(LoginModel model)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var serialized = JsonConvert.SerializeObject(model);

                var json = await client.PostAsync(new Uri($"/api/login"), new StringContent(serialized, Encoding.UTF8, "application/json"));
                if (json.IsSuccessStatusCode)
                {
                    var response = json.Content.ReadAsStringAsync().Result;
                    return AuthModel = JsonConvert.DeserializeObject<AuthModel>(response);
                }
                else
                {
                    throw new BusinessException("Email or Password incorrect!");
                }
            }

            return null;
        }

        public async Task<AuthModel> Register(RegisterModel model)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var serialized = JsonConvert.SerializeObject(model);
                StringContent content = new StringContent(serialized.ToString(), Encoding.UTF8, "application/json");

                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri($"/api/register"),
                    Method = HttpMethod.Post,
                    Content = content
                };

                var json = await client.SendAsync(request);
                if (json.IsSuccessStatusCode)
                {
                    var response = json.Content.ReadAsStringAsync().Result;
                    return AuthModel = JsonConvert.DeserializeObject<AuthModel>(response);
                }
                else
                {
                    throw new BusinessException("Email already exist!");
                }
            }

            return null;
        }
    }
}
