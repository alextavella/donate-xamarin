﻿using System;
using System.Threading.Tasks;
using System.Net.Http;

using Newtonsoft.Json;
using Plugin.Connectivity;

using XF.Donate.Services.Contracts;
using XF.Donate.Models;

namespace XF.Donate.Services
{
    public class CategoryService : ICategoryService
    {
        HttpClient client;

        CategoriesModel categories;

        public CategoryService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri($"{App.BackendUrl}");

            categories = new CategoriesModel();
        }

        public async Task<CategoriesModel> List()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var json = await client.GetStringAsync($"/api/category");
                categories = await Task.Run(() => JsonConvert.DeserializeObject<CategoriesModel>(json));
            }

            return categories;
        }
    }
}
