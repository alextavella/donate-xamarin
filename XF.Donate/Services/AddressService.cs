﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Headers;

using XF.Donate.Services.Exceptions;
using XF.Donate.Services.Contracts;
using XF.Donate.Models;

using Newtonsoft.Json;
using Plugin.Connectivity;

namespace XF.Donate.Services
{
    public class AddressService : IAddressService
    {
        HttpClient client;

        public static AddressesModel AddressesModel { get; private set; }

        public AddressService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri($"{App.BackendUrl}");
        }

        public async Task<AddressesModel> List()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri($"/api/address"),
                    Method = HttpMethod.Get
                };
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", $"{AuthService.AccessToken()}");

                var json = await client.SendAsync(request);
                if (json.IsSuccessStatusCode)
                {
                    var response = json.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<AddressesModel>(response);

                } else if(json.StatusCode == System.Net.HttpStatusCode.Forbidden) {
                    throw new ForbiddenException();
                }
            }

            return null;
        }

        public async Task<AddressModel> Register(AddressModel model)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var serialized = JsonConvert.SerializeObject(model);
                StringContent content = new StringContent(serialized.ToString(), Encoding.UTF8, "application/json");

                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri($"/api/address"),
                    Method = HttpMethod.Post,
                    Content = content
                };
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", $"{AuthService.AccessToken()}");

                var json = await client.SendAsync(request);
                if (json.IsSuccessStatusCode)
                {
                    var response = json.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<AddressModel>(response);
                }
                else if (json.StatusCode == System.Net.HttpStatusCode.Forbidden)
                {
                    throw new ForbiddenException();
                }
            }

            return null;
        }

        public async Task Remove(string id)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri($"/api/address/{id}"),
                    Method = HttpMethod.Delete
                };
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", $"{AuthService.AccessToken()}");

                var json = await client.SendAsync(request);
                if (json.IsSuccessStatusCode)
                {
                    var response = json.Content.ReadAsStringAsync().Result;
                }
                else if (json.StatusCode == System.Net.HttpStatusCode.Forbidden)
                {
                    throw new ForbiddenException();
                }
            }
        }

        public async Task<AddressCepModel> SearchByCep(string cep)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri($"/api/address/cep/{cep}"),
                    Method = HttpMethod.Get
                };

                var json = await client.SendAsync(request);
                if (json.IsSuccessStatusCode)
                {
                    var response = json.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<AddressCepModel>(response);
                }
            }

            return null;
        }
    }
}
