﻿using System.Threading.Tasks;

using XF.Donate.Models;

namespace XF.Donate.Services.Contracts
{
    public interface ICategoryService
    {
        Task<CategoriesModel> List();
    }
}
