﻿using System.Threading.Tasks;

using XF.Donate.Models;

namespace XF.Donate.Services.Contracts
{
    public interface IPhoneService
    {
        Task<PhonesModel> List();
        Task<PhoneModel> Register(PhoneModel model);
        Task Remove(string id);
    }
}
