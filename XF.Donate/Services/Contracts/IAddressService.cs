﻿using System.Threading.Tasks;

using XF.Donate.Models;

namespace XF.Donate.Services.Contracts
{
    public interface IAddressService
    {
        Task<AddressesModel> List();
        Task<AddressModel> Register(AddressModel model);
        Task Remove(string id);
        Task<AddressCepModel> SearchByCep(string cep);
    }
}
