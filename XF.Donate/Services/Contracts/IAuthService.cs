﻿using System.Threading.Tasks;

using XF.Donate.Models;

namespace XF.Donate.Services.Contracts
{
    public interface IAuthService
    {
        Task<AuthModel> Login(string email, string password);
        Task<AuthModel> Login(LoginModel model);
        Task<AuthModel> Register(RegisterModel model);
    }
}
