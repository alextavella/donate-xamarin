﻿using System.Collections.Generic;
using System.Threading.Tasks;

using XF.Donate.Models;

namespace XF.Donate.Services.Contracts
{
    public interface IProductService
    {
        Task<ProductsModel> List();
        Task<ProductRegisterModel> Register(ProductRegisterModel model);
    }
}
