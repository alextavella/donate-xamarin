﻿using System;
using System.Text;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net.Http;

using Newtonsoft.Json;
using Plugin.Connectivity;

using XF.Donate.Services.Exceptions;
using XF.Donate.Services.Contracts;
using XF.Donate.Models;

namespace XF.Donate.Services
{
    public class PhoneService : IPhoneService
    {
        HttpClient client;

        PhonesModel phones;

        public PhoneService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri($"{App.BackendUrl}");

            phones = new PhonesModel();
        }

        public async Task<PhonesModel> List()
        {
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri($"/api/phone"),
                Method = HttpMethod.Get
            };
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", $"{AuthService.AccessToken()}");

            var json = await client.SendAsync(request);
            if (json.IsSuccessStatusCode)
            {
                var response = json.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<PhonesModel>(response);

            }
            else if (json.StatusCode == System.Net.HttpStatusCode.Forbidden)
            {
                throw new ForbiddenException();
            }

            return phones;
        }

        public async Task<PhoneModel> Register(PhoneModel model)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var serialized = JsonConvert.SerializeObject(model);
                StringContent content = new StringContent(serialized.ToString(), Encoding.UTF8, "application/json");

                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri($"/api/phone"),
                    Method = HttpMethod.Post,
                    Content = content
                };
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", $"{AuthService.AccessToken()}");

                var json = await client.SendAsync(request);
                if (json.IsSuccessStatusCode)
                {
                    var response = json.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<PhoneModel>(response);
                }
                else if (json.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    throw new BusinessException("Number deny");
                }
                else if (json.StatusCode == System.Net.HttpStatusCode.Forbidden)
                {
                    throw new ForbiddenException();
                }
                else
                {
                    throw new BusinessException("Failed!");
                }
            }

            return null;
        }

        public async Task Remove(string id)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri($"/api/phone/{id}"),
                    Method = HttpMethod.Delete
                };
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", $"{AuthService.AccessToken()}");

                var json = await client.SendAsync(request);
                if (json.IsSuccessStatusCode)
                {
                    var response = json.Content.ReadAsStringAsync().Result;
                }
                else if (json.StatusCode == System.Net.HttpStatusCode.Forbidden)
                {
                    throw new ForbiddenException();
                }
            }
        }
    }
}
