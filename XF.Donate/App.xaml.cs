﻿using XF.Donate.Services;

using Xamarin.Forms;

namespace XF.Donate
{
    public partial class App : Application
    {
        public static bool IsDebug = true;
        public static bool UseMockDataStore = true;
        public static string BackendUrl = "http://alextavellaapp-com.umbler.net/api";

        public App()
        {
            InitializeComponent();

            // Store
            if (UseMockDataStore)
                DependencyService.Register<MockDataStore>();
            else
                DependencyService.Register<CloudDataStore>();

            // Services
            DependencyService.Register<AuthService>();
            DependencyService.Register<CategoryService>();
            DependencyService.Register<PhoneService>();
            DependencyService.Register<AddressService>();
            DependencyService.Register<ProductService>();

            // Main
            if (Device.RuntimePlatform == Device.iOS)
                MainPage = new MainPage();
            else
                MainPage = new NavigationPage(new MainPage());
        }
    }
}
